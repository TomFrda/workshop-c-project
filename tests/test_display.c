/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** criterion tests
*/


#include "tests.h"

Test(display, basic_display, .init = redirect_all_std)
{
    char *map[] = {
        "  |  ",
        " ||| ",
        "|||||",
        NULL
    };
    char expected[] = {
        "*******\n"
        "*  |  *\n"
        "* ||| *\n"
        "*|||||*\n"
        "*******\n"
    };

    display_map(5, (const char **)map);
    cr_assert_stdout_eq_str(expected);
}
