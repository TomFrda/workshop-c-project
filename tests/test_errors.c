/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** criterion tests
*/


#include "tests.h"

Test(errors, error_too_less_args, .init = redirect_all_std)
{
    cr_assert_eq(check_errors(2, NULL), ERROR);
    cr_assert_stderr_eq_str(ERROR_NBR_ARGS);
}

Test(errors, error_too_much_args, .init = redirect_all_std)
{
    cr_assert_eq(check_errors(4, NULL), ERROR);
    cr_assert_stderr_eq_str(ERROR_NBR_ARGS);
}

Test(errors, error_invalid_arg_strings_before, .init = redirect_all_std)
{
    char *av[] = {"./matchstick", "()str", "21"};

    cr_assert_eq(check_errors(3, (const char **)av), ERROR);
    cr_assert_stderr_eq_str(ERROR_INVALID_ARGS);
}

Test(errors, error_invalid_arg_strings_after, .init = redirect_all_std)
{
    char *av[] = {"./matchstick", "str", "21"};

    cr_assert_eq(check_errors(3, (const char **)av), ERROR);
    cr_assert_stderr_eq_str(ERROR_INVALID_ARGS);
}

Test(errors, error_invalid_arg_lower_lines, .init = redirect_all_std)
{
    char *av[] = {"./matchstick", "1", "21"};

    cr_assert_eq(check_errors(3, (const char **)av), ERROR);
    cr_assert_stderr_eq_str(ERROR_NBR_LINES);
}

Test(errors, error_invalid_arg_higher_lines, .init = redirect_all_std)
{
    char *av[] = {"./matchstick", "100", "21"};

    cr_assert_eq(check_errors(3, (const char **)av), ERROR);
    cr_assert_stderr_eq_str(ERROR_NBR_LINES);
}

Test(errors, error_invalid_arg_lower_maches, .init = redirect_all_std)
{
    char *av[] = {"./matchstick", "10", "0"};

    cr_assert_eq(check_errors(3, (const char **)av), ERROR);
    cr_assert_stderr_eq_str(ERROR_NBR_MATCHES);
}

Test(errors, no_error_in_args)
{
    char *av[] = {"./matchstick", "10", "5"};

    cr_assert_eq(check_errors(3, (const char **)av), SUCCESS);
}
