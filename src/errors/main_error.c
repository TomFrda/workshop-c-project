/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** Errors management
*/

#include "macros.h"
#include "mystring.h"
#include "mymath.h"

ret_t check_errors(int ac, const char **av)
{
    int nbr;

    if (ac != 3)
        return my_print_int_error(ERROR_NBR_ARGS, ERROR_FD, ERROR);
    for (int i = 1; i < ac; i++) {
        for (int y = 0; av[i][y]; y++) {
            if (av[i][y] < '0' || av[i][y] > '9')
                return my_print_int_error(ERROR_INVALID_ARGS, ERROR_FD, ERROR);
        }
    }
    nbr = my_getnbr(av[1]);
    if (nbr < 2 || nbr > 99)
        return my_print_int_error(ERROR_NBR_LINES, ERROR_FD, ERROR);
    nbr = my_getnbr(av[2]);
    if (nbr < 1)
        return my_print_int_error(ERROR_NBR_MATCHES, ERROR_FD, ERROR);
    return SUCCESS;
}
