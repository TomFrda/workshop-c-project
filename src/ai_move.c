/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** matchstick
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mystring.h"
#include "mymath.h"
#include "usage.h"
#include "errors.h"
#include "matchstick.h"

short get_sticks_nbr(const char *line)
{
    short sticks = 0;

    for (int i = 0; line[i]; i++) {
        if (line[i] == STICK_CHAR)
            sticks++;
    }
    return sticks;
}

void remove_line(short sticks, short matches, short line, char **map_line)
{
    if (sticks > matches)
        do_remove(map_line, line, matches);
    else
        do_remove(map_line, line, sticks);
}

void change_line(short sticks, short matches, short line, char **map_line)
{
    if (sticks % (matches + 1) != 1)
        do_remove(map_line, line, (sticks - 1) % (matches + 1));
    else
        do_remove(map_line, line, 1);
}

void do_ai_move(game_t *game)
{
    short line = 0;
    short tmp;
    short sticks = 500;
    short lines_nbr = 0;

    my_putstr("\nAI's turn...\n");
    for (short i = 0; (game->map)[i]; i++) {
        tmp = get_sticks_nbr((game->map)[i]);
        if (tmp)
            lines_nbr++;
        if (tmp && tmp < sticks) {
            sticks = tmp;
            line = i;
        }
    }
    my_putstr("AI");
    if (lines_nbr > 1)
        remove_line(sticks, game->matches, line + 1, &((game->map)[line]));
    else
        change_line(sticks, game->matches, line + 1, &((game->map)[line]));
    game->cur_player = USER;
}
