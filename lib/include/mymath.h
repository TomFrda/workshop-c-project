/*
** EPITECH PROJECT, 2020
** Mymath
** File description:
** Prototypes of the math functions
*/

#ifndef MYMATH_H
#define MYMATH_H

int my_isneg(int nb);
int my_absolute(int nbr);
int my_getnbr(char const *str);
void my_sort_int_array(int *tab, int size);
int my_compute_power_rec(int nb, int power);
int my_compute_square_root(int nb);
int my_is_prime(int nb);
int my_find_prime_sup(int nb);
int is_alpha(char c);

#endif /* MYMATH_H */
