/*
** EPITECH PROJECT, 2021
** my_print_errors
** File description:
** Print error functions
*/

#include <unistd.h>
#include "mystring.h"

//str : the string to display on error
//fd : the file where you want to write (default -> 2)
//ret : The int you want to return
int my_print_int_error(const char *str, int fd, int ret)
{
    if (fd < 0)
        fd = 2;
    write(fd, str, my_strlen(str));
    return ret;
}

//str : the string to display on error
//fd : the file where you want to write (default -> 2)
//ret : The value you want to return
void *my_print_error(const char *str, int fd, void *ret)
{
    if (fd < 0)
        fd = 2;
    write(fd, str, my_strlen(str));
    return ret;
}
