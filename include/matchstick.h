/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** Main prototypes of Matchstick project
*/

#ifndef MAIN_H
#define MAIN_H

#include "macros.h"
#include "game_struct.h"

ret_t matchstick(int ac, const char **av);
ret_t init_gamestruct(const char **av, game_t *game);
void get_player_reads(game_t *game);
void do_ai_move(game_t *game);
void do_remove(char **map_line, short line, short matches);
void free_map(char **map);

#endif /* MAIN_H */
