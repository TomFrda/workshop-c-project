/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** Prototypes and includes for citerion tests
*/

#ifndef CRITERION_TESTS_H
#define CRITERION_TESTS_H

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "game_struct.h"

// Testing utils functions
void redirect_all_std(void);

// Usage check functions
void print_usage(void);
short check_usage(int ac, const char **av);

// Errors check functions
ret_t check_errors(int ac, const char **av);

// Init and free map functions
void fill_map_line(game_t *game, short i);
ret_t init_gamestruct(const char **av, game_t *game);
void free_map(char **map);

// Display function
void display_map(short cols, const char **map);

// User interaction functions
ret_t check_line(short lines, const char *line);
short get_player_line(short lines);
ret_t check_matches(short matches, short free_in_line, const char *line);
short get_player_matches(short matches, const char *map_line);
void get_player_reads(game_t *game);

// AI functions
short get_sticks_nbr(const char *line);
void remove_line(short sticks, short matches, short line, char **map_line);
void change_line(short sticks, short matches, short line, char **map_line);
void do_ai_move(game_t *game);

// Main game functions
ret_t check_endgame(const char **map, ret_t player);
void do_remove(char **map_line, short line, short matches);
ret_t launch_game(game_t game);
ret_t matchstick(int ac, const char **av);

#endif /* CRITERION_TESTS_H */
